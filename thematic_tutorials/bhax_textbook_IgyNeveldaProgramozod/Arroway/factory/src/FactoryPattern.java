public class FactoryPattern {
    public static void main(String[] args) {
        HeadphoneFactory headphoneFactory = new HeadphoneFactory();

        Headphone first = headphoneFactory.getHeadphone("Sony");
        first.makeSound();

        Headphone second = headphoneFactory.getHeadphone("JBL");
        second.makeSound();
    }
}
