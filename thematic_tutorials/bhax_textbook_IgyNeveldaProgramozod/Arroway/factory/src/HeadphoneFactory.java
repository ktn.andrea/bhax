public class HeadphoneFactory {
    public Headphone getHeadphone(String Brand) {
        if (Brand.equals("Sony")) {
            return new Sony();
        }
        else if (Brand.equals("JBL")) {
            return new JBL();
        }
        return null;
    }
}
