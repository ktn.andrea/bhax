public abstract class AbstractFactory {
    abstract Headphone getHeadphone(String Brand) ;
}
