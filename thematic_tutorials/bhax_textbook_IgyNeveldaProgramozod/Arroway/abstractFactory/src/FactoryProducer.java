public class FactoryProducer {
    public static AbstractFactory getFactory(boolean wireless){
        if(wireless){
            return new WirelessHeadphoneFactory();
        }
        else {
            return new HeadphoneFactory();
        }
    }
}
