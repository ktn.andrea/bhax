public class AbstractFactoryPattern {
    public static void main(String[] args) {
        AbstractFactory Factory1 = FactoryProducer.getFactory(false);
        Headphone first = Factory1.getHeadphone("Sony");
        first.makeSound();

        Headphone second = Factory1.getHeadphone("JBL");
        second.makeSound();

        AbstractFactory Factory2 = FactoryProducer.getFactory(true);
        Headphone third = Factory2.getHeadphone("Sony");
        third.makeSound();
        Headphone fourth = Factory2.getHeadphone("JBL");
        fourth.makeSound();
    }

}
