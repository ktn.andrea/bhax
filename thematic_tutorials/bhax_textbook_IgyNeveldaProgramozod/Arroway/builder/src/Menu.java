public class Menu implements MenuPlan{
    private String breakfast;
    private String lunch;
    private String dinner;

    public void setBreakfast(String breakfast)
    {
        this.breakfast = breakfast;
    }
    public void setLunch(String lunch)
    {
        this.lunch = lunch;
    }
    public void setDinner(String dinner)
    {
        this.dinner = dinner;
    }

    public void displayMenu(){
        System.out.println(this.breakfast);
        System.out.println(this.lunch);
        System.out.println(this.dinner);
    }
}
