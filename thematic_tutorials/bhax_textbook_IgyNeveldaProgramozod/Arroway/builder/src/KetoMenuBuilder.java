public class KetoMenuBuilder implements MenuBuilder {
    private Menu menu;

    public KetoMenuBuilder()
    {
        this.menu = new Menu();
    }
    public void buildBreakfast()
    {
        menu.setBreakfast("Eggs");
    }
    public void buildLunch()
    {
        menu.setLunch("Steak");
    }
    public void buildDinner()
    {
        menu.setDinner("Chicken curry");
    }

    public Menu getMenu()
    {
        return this.menu;
    }
}
