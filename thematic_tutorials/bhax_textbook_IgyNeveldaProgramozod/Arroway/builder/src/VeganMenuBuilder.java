public class VeganMenuBuilder implements MenuBuilder {
    private Menu menu;

    public VeganMenuBuilder()
    {
        this.menu = new Menu();
    }
    public void buildBreakfast()
    {
        menu.setBreakfast("Oats");
    }
    public void buildLunch()
    {
        menu.setLunch("Veggie burger");
    }
    public void buildDinner()
    {
        menu.setDinner("Salad");
    }

    public Menu getMenu()
    {
        return this.menu;
    }
}
