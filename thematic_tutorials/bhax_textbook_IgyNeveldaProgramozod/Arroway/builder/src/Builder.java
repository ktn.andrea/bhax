public class Builder {
    public static void main(String[] args)
    {
        MenuBuilder veganMenu = new VeganMenuBuilder();
        MenuPlanner menuPlanner = new MenuPlanner(veganMenu);
        menuPlanner.makeMenu();
        Menu menu = menuPlanner.getMenu();
        System.out.println("Vegan menu: ");
        menu.displayMenu();

        MenuBuilder ketoMenu = new KetoMenuBuilder();
        menuPlanner = new MenuPlanner(ketoMenu);
        menuPlanner.makeMenu();
        Menu menu2 = menuPlanner.getMenu();
        System.out.println("Keto menu: ");
        menu2.displayMenu();
    }
}
