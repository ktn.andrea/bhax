public interface MenuPlan {

    public void setBreakfast(String breakfast);
    public void setLunch(String lunch);
    public void setDinner(String dinner);

}
