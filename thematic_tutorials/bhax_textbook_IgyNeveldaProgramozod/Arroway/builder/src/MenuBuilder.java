public interface MenuBuilder {
    public void buildBreakfast();
    public void buildLunch();
    public void buildDinner();

    public Menu getMenu();
}
