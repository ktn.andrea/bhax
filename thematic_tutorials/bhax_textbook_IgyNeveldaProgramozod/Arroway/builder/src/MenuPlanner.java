public class MenuPlanner {
    private MenuBuilder menuBuilder;

    public MenuPlanner(MenuBuilder menuBuilder)
    {
        this.menuBuilder = menuBuilder;
    }

    public Menu getMenu()
    {
        return this.menuBuilder.getMenu();
    }

    public void makeMenu()
    {
        this.menuBuilder.buildBreakfast();
        this.menuBuilder.buildLunch();
        this.menuBuilder.buildDinner();
    }
}
