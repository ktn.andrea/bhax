#include <iostream>
#include <vector>
using namespace std;

template<typename T>
class CustomAlloc{
public:
    using size_type= size_t;
    using value_type= T;
    using pointer = T*;
    using const_pointer = const T*;
    
    pointer allocate( size_type n){
    cout << "Allocating "<< n << " objects of "<< n*sizeof (T)<< " bytes. "<< endl;
    
    return reinterpret_cast<T*>(new char[n*sizeof(T)]);
    }
    
    void deallocate (pointer p, size_type n){
        cout << "Deallocating " << n << " objects of "<< n*sizeof (T) << " bytes. " << endl;
        
        delete[] reinterpret_cast<char *>(p);
    }
};

int main(){
    
    vector<int, CustomAlloc<int>> numbers;
    numbers.push_back(1);
    numbers.push_back(2);
    
    return 0;
}
