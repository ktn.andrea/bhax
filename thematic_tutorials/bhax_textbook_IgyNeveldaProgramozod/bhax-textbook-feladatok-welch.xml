<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Welch!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>
    <section>
        <title>Első osztályom</title>
        <para>
            Valósítsd meg C++-ban és Java-ban az módosított polártranszformációs algoritmust! A matek háttér 
            teljesen irreleváns, csak annyiban érdekes, hogy az algoritmus egy számítása során két normálist
            számol ki, az egyiket elspájzolod és egy további logikai taggal az osztályban jelzed, hogy van vagy
            nincs eltéve kiszámolt szám.
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/Y9_p9VtKd0I">https://youtu.be/Y9_p9VtKd0I</link>
        </para>
        <para>
            Megoldás forrása: (Bátfai Norbert) </para>
        <para>
            <link xlink:href="Welch/polargen.cpp">bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Welch/polargen.cpp</link>
        </para>
        <para>
             <link xlink:href="Welch/polargen.h">bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Welch/polargen.h</link>
        </para> 
        <para>
             <link xlink:href="Welch/polargenteszt.cpp">bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Welch/polargenteszt.cpp</link>
        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat... térj ki arra is, hogy a JDK forrásaiban a Sun programozói
            pont úgy csinálták meg ahogyan te is, azaz az OO nemhogy nem nehéz, hanem éppen természetes neked!
        </para>    
        <para>
             Az alábbi program célja a véltelen számok generálása. A C++ verzióban három forrásállománnyal dolgozunk. A <command>polargenteszt.cpp</command> állomány tartalmazza a main-t. Itt egy for ciklust találunk, ahol meg tudjuk szabni, hány random számot szeretnénk. Annyi számot fogunk kapni eredményül, ahányszor lefut a ciklus.
             A program alapvetően két random számot generál egy lépésben. Az egyikkel mindig visszatér, a másikat pedig eltárolja a <command>tarolt</command> változóban.
             Ez azért hatékony, mert a következő szám kérésénél már rendelkezésünkre áll az előzőleg előállított, még nem felhasznált szám. 
        </para>
        <para>
             A <command>polargen.h</command> állomány a PolarGen osztályt tartalmazza. Ebben található egy konstruktor és egy destruktor. A konstruktorban meghatározzuk, hogy nincs tárolt elem, illetve inicialzáljuk a random szám generátort. Ehhez a time(NULL)-t használjuk, ami minden egyes meghívásnál más értékkel tér vissza. Tehát ezzel biztosítjuk, hogy ne ugyanazt a random számot kapjuk minden alkalommal. A <command>polargen.cpp</command> állományban a <command>::</command> operátorral tudunk hivatkozni az a másik állományban lévő osztály elemére. Ezt az operátort többször is alkalmazzuk ebben a feladatban.
             A <command>kovetkezo()</command> függvényben alapvetően egy elágazást találunk: ha van tárolt elem, akkor azzal térünk vissza. Ha nincs, akkor elvégezzük a két random szám generálását, az egyikkel visszatérünk, a másikat pedig a <command>tarolt</command> változónak adjuk át.
        </para>
        <para>
            Fontos, hogy mindkét cpp állományban include-olnunk kell a polargen.h állományt, mivel mindkét kódban hivatkozunk a tartalmára.
        </para>
        <para>
             Megoldás forrása: (Bátfai Norbert) <link xlink:href="Welch/polargen.java">bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Welch/polargen.java</link>
        </para>
        <para>
            A Java verzióban a működési elv ugyanaz, viszont a kód sokkal átláthatóbb. Egy állománnyal dolgozunk és gyakorlatilag csak néhány szót kellett átírnunk. Osztályon belül definiáljuk a metódusokat, illetve a random szám generálása is egyszerűbben kivitelezhető. Nem használunk destruktort, mivel Java-ban a garbage collector automatikusan felszabadítja a már nem használt objektum által lefoglalt memóriát. 
        </para>
        <para>
            Az alábbi kódrészlet a <command>Random.java</command> állományból származik, amit a gépünkön a Java mappában található <command>src.zip</command>-ből érhetünk el. Ezt a részletet tanulmányozva láthatjuk, hogy ugyanazt a módszert alkalmaztuk a programunkban, mint a nyelv készítői.
            Az alábbi kód azért hatékony, mert úgy generál két számot, hogy csak egyszer használja fel a <command>StrictMath.sqrt()</command> és a <command>StrictMath.log()</command> metódusokat. Ezáltal a páratlan iterációknál nem kell mégegyszer elvégezni a számítást, mert már rendelkezésre áll az előzőből megmaradt, még nem használt szám.
        </para> 
<programlisting language="c++"><![CDATA[
synchronized public double nextGaussian() {
        // See Knuth, ACP, Section 3.4.1 Algorithm C.
        if (haveNextNextGaussian) {
            haveNextNextGaussian = false;
            return nextNextGaussian;
        } else {
            double v1, v2, s;
            do {
                v1 = 2 * nextDouble() - 1; // between -1 and 1
                v2 = 2 * nextDouble() - 1; // between -1 and 1
                s = v1 * v1 + v2 * v2;
            } while (s >= 1 || s == 0);
            double multiplier = StrictMath.sqrt(-2 * StrictMath.log(s)/s);
            nextNextGaussian = v2 * multiplier;
            haveNextNextGaussian = true;
            return v1 * multiplier;
        }
    }]]></programlisting> 
        
<para>
    További részlet a Random.java állományból:
    </para>
    <programlisting language="c++"><![CDATA[
     * This uses the <i>polar method</i> of G. E. P. Box, M. E. Muller, and
     * G. Marsaglia, as described by Donald E. Knuth in <i>The Art of
     * Computer Programming</i>, Volume 3: <i>Seminumerical Algorithms</i>,
     * section 3.4.1, subsection C, algorithm P. Note that it generates two
     * independent values at the cost of only one call to {@code StrictMath.log}
     * and one call to {@code StrictMath.sqrt}.]]></programlisting> 
    </section>        

    <section>
        <title>LZW</title>
        <para>
            Valósítsd meg C-ben az LZW algoritmus fa-építését!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/17l7HwZWtuU">https://youtu.be/17l7HwZWtuU</link>
        </para>
        <para>
            Megoldás forrása: (Bátfai Norbert)
        </para>
        <para>
            <link xlink:href="Welch/z.c">bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Welch/z.c</link>     
        </para>
        <para>
            Az LZW egy veszteségmentes tömörítő algoritmus, melyet a mai is napig széleskörben használnak, főleg <command>gif</command> és <command>pdf</command> fájlok esetében. Eredetileg a Unix egy segédprogramjának algoritmusaként terjedt el. A tömörítés során eltárolt adatokat fa struktúrában tudjuk ábrázolni.
        </para>
        <para>
            Az alábbi program a bináris fa építését szemlélteti. Ebben az esetben a gyökeret nem tekintjük a fa részének, azaz a mélység számításakor a gyökeret nem vesszük figyelembe. Fordításhoz szükséges a <command>-lm</command> használata. 
        </para>
 <programlisting language="c++"><![CDATA[
// Copyright (C) 2011, Bátfai Norbert, nbatfai@inf.unideb.hu, nbatfai@gmail.com

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

typedef struct binfa
{
  int ertek;
  struct binfa *bal_nulla;
  struct binfa *jobb_egy;

} BINFA, *BINFA_PTR;

BINFA_PTR
uj_elem ()
{
  BINFA_PTR p;//új elem létrehozása

  if ((p = (BINFA_PTR) malloc (sizeof (BINFA))) == NULL) //ha nem sikerült helyet foglalni
    {
      perror ("memoria");//error, nincs elég memória
      exit (EXIT_FAILURE);
    }
  return p;
}

extern void kiir (BINFA_PTR elem);
extern void ratlag (BINFA_PTR elem);
extern void rszoras (BINFA_PTR elem);
extern void szabadit (BINFA_PTR elem);

int
main (int argc, char **argv)
{
  char b;

  BINFA_PTR gyoker = uj_elem ();//gyökérelem létrehozása
  gyoker->ertek = '/';//gyökér értékadás(karakter)
  gyoker->bal_nulla = gyoker->jobb_egy = NULL;
  BINFA_PTR fa = gyoker;//mutató gyökérre állítása
  long max=0;
while (read (0, (void *) &b, 1))//karakterek beolvasása
    {
//      write (1, &b, 1);
      if (b == '0')//ha a karakter 0
	{
	  if (fa->bal_nulla == NULL)//ha nincs bal oldali elem
	    {
	      fa->bal_nulla = uj_elem ();//létrehozás
	      fa->bal_nulla->ertek = 0;
	      fa->bal_nulla->bal_nulla = fa->bal_nulla->jobb_egy = NULL;
	      fa = gyoker;
	    }
	  else//(ha már van bal oldali elem)
	    {
	      fa = fa->bal_nulla;
	    }
	}
      else//ha a karaktern nem 0
	{
	  if (fa->jobb_egy == NULL)//ha nincs jobb oldali elem
	    {
	      fa->jobb_egy = uj_elem ();//létrehozás
	      fa->jobb_egy->ertek = 1;
	      fa->jobb_egy->bal_nulla = fa->jobb_egy->jobb_egy = NULL;
	      fa = gyoker;
	    }
	  else
	    {
	      fa = fa->jobb_egy;
	    }
	}
    }

  printf ("\n");
  kiir (gyoker);

  extern int max_melyseg, atlagosszeg, melyseg, atlagdb;
  extern double szorasosszeg, atlag;

  printf ("melyseg=%d\n", max_melyseg - 1);

  /* Átlagos ághossz kiszámítása */
  atlagosszeg = 0;
  melyseg = 0;
  atlagdb = 0;
  ratlag (gyoker);
  // atlag = atlagosszeg / atlagdb;
  // (int) / (int) "elromlik", ezért casoljuk
  // K&R tudatlansági védelem miatt a sok () :)
  atlag = ((double) atlagosszeg) / atlagdb;

  /* Ághosszak szórásának kiszámítása */
  atlagosszeg = 0;
  melyseg = 0;
  atlagdb = 0;
  szorasosszeg = 0.0;

  rszoras (gyoker);

  double szoras = 0.0;

  if (atlagdb - 1 > 0)
    szoras = sqrt (szorasosszeg / (atlagdb - 1));
  else
    szoras = sqrt (szorasosszeg);

  printf ("altag=%f\nszoras=%f\n", atlag, szoras);

  szabadit (gyoker);
}


 // a Javacska ONE projekt Hetedik Szem/TudatSzamitas.java mintajara
 // http://sourceforge.net/projects/javacska/
 // az atlag() hivasakor is inicializalni kell oket, a
 // a rekurziv bejaras hasznalja
int atlagosszeg = 0, melyseg = 0, atlagdb = 0;

void
ratlag (BINFA_PTR fa)
{

  if (fa != NULL)//ha létrejött a fa
    {
      ++melyseg;
      ratlag (fa->jobb_egy);
      ratlag (fa->bal_nulla);
      --melyseg;

      if (fa->jobb_egy == NULL && fa->bal_nulla == NULL)
	{

	  ++atlagdb;
	  atlagosszeg += melyseg;

	}

    }

}

 // a Javacska ONE projekt Hetedik Szem/TudatSzamitas.java mintajara
 // http://sourceforge.net/projects/javacska/
 // az atlag() hivasakor is inicializalni kell oket, a
 // a rekurziv bejaras hasznalja
double szorasosszeg = 0.0, atlag = 0.0;

void
rszoras (BINFA_PTR fa)
{

  if (fa != NULL)
    {
      ++melyseg;
      rszoras (fa->jobb_egy);
      rszoras (fa->bal_nulla);
      --melyseg;

      if (fa->jobb_egy == NULL && fa->bal_nulla == NULL)
	{

	  ++atlagdb;
	  szorasosszeg += ((melyseg - atlag) * (melyseg - atlag));

	}

    }

}

//static int melyseg = 0;
int max_melyseg = 0;

void
kiir (BINFA_PTR elem)
{
  if (elem != NULL)
    {
      ++melyseg;
      if (melyseg > max_melyseg)
	max_melyseg = melyseg;
      kiir (elem->jobb_egy);
      // ez a postorder bejáráshoz képest
      // 1-el nagyobb mélység, ezért -1
      for (int i = 0; i < melyseg; ++i)
	printf ("---");
      printf ("%c(%d)\n", elem->ertek < 2 ? '0' + elem->ertek : elem->ertek,
	      melyseg - 1);
      kiir (elem->bal_nulla);
      --melyseg;
    }
}

void
szabadit (BINFA_PTR elem)
{
  if (elem != NULL)
    {
      szabadit (elem->jobb_egy);
      szabadit (elem->bal_nulla);
      free (elem);
    }
}
]]></programlisting> 
    <para>
        Bináris fával dolgozunk, tehát minden elemnek legfeljebb két gyermeke lehet, 0 és 1. Egy új elem beszúrásakor nemcsak az elem értékét adjuk meg, hanem létrehozunk két mutatót is. Ezeket struktúrában definiáljuk. Ebben a programban az elemek szűrését úgy végezzük, hogy az elágazásban csak azt a feltételt vizsgáljuk, hogy az elem nulla-e vagy nem. Tehát az '1' karakterek helyett bármi mást is írhatnánk.
    </para>
    <para>
        Első lépésben létrehozzuk a gyökérelemet. Ezt követően elkezdjük beolvasni a karaktereket. Ha az első elem 0, akkor a gyökér bal oldali gyermeke lesz. Az elem beszúrása után a fa mutatót visszaállítjuk a gyökérre. Tovább olvassuk a karaktereket, és végigmegyünk a fán. A mutató mozgatásával vizsgáljuk az aktuális elemeket, majd ha NULL-hoz érünk, akkor beszúrjuk az új értéket.
    </para>
    <para>
        A fa bejárása inorder módszerrel történik, tehát a sorrend: bal oldali részfa, gyökér, jobb oldali részfa. Végül a fa átlagát és szórását rekurzív függvények segítségével számoljuk ki.
    </para>
     <figure>
            <title>Fa inorder</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/z.png" scale="40" />
                </imageobject>
                <textobject>
                    <phrase>Fa inorder</phrase>
                </textobject>
            </mediaobject>
        </figure>  
    </section>        
        
    <section>
        <title>Fabejárás</title>
        <para>
            Tutoriáltam: <link xlink:href="https://gitlab.com/bh_konyv/bhax">Salánki László Balázs</link>
            </para>
        <para>
            Járd be az előző (inorder bejárású) fát pre- és posztorder is!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/arSOn0bG-Ng">https://youtu.be/arSOn0bG-Ng</link>
        </para>
        <para>
            Megoldás forrása: (Bátfai Norbert)
        </para>
        <para> 
            <link xlink:href="Welch/z_preorder.c">bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Welch/z_preorder.c</link>     
        </para>
        <para> 
            <link xlink:href="Welch/z_postorder.c">bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Welch/z_postorder.c</link>     
        </para>
        <para>
            A fent találtható programban a fabejárás inorder történik, tehát először a bal oldali részfa, majd a gyökér, végül a jobb oldali részfa.
            A fa felépítése nem változik, csak más sorrendben írjuk ki az elemeket. Tehát ahhoz, hogy ezt módosítsuk, elegendő a <command>kiir()</command> függvényt átírnunk.
        </para>
        <para>
             Preorder bejárás: gyökér, bal oldali részfa, jobb oldali részfa feldolgozása.
        </para>
        
<programlisting language="c++"><![CDATA[
void kiir (BINFA_PTR elem)
{
  if (elem != NULL)
    {
      ++melyseg;
      if (melyseg > max_melyseg)
	max_melyseg = melyseg;
      for (int i = 0; i < melyseg; ++i)
	printf ("---");
      printf ("%c(%d)\n", elem->ertek < 2 ? '0' + elem->ertek : elem->ertek,
	      melyseg - 1);
       kiir (elem->jobb_egy);
      kiir (elem->bal_nulla);
      --melyseg;
    }
}
]]></programlisting> 
 <figure>
            <title>Fa preorder</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/z_preorder.png" scale="40" />
                </imageobject>
                <textobject>
                    <phrase>Fa preorder</phrase>
                </textobject>
            </mediaobject>
        </figure>  
         <para>
             Postorder bejárás: bal oldali részfa, jobb oldali részfa, végül a gyökér feldolgozása.
        </para>
        <para>
             A gyökér('/' karakter) helyzetéből könnyen meg tudjuk figyelni a kimenetből, hogy jól dolgoztunk-e.
        </para>
<programlisting language="c++"><![CDATA[
void kiir (BINFA_PTR elem)
{
  if (elem != NULL)
    {
      ++melyseg;
      if (melyseg > max_melyseg)
	max_melyseg = melyseg;
      kiir (elem->bal_nulla);
      kiir (elem->jobb_egy);
      for (int i = 0; i < melyseg; ++i)
	printf ("---");
      printf ("%c(%d)\n", elem->ertek < 2 ? '0' + elem->ertek : elem->ertek,
	      melyseg - 1);
      --melyseg;
    }
}
]]></programlisting> 
 <figure>
            <title>Fa postorder</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/z_postorder.png" scale="40" />
                </imageobject>
                <textobject>
                    <phrase>Fa postorder</phrase>
                </textobject>
            </mediaobject>
        </figure>  
    </section>        
                        
    <section>
        <title>Tag a gyökér</title>
        <para>
            Az LZW algoritmust ültesd át egy C++ osztályba, legyen egy Tree és egy beágyazott Node
            osztálya. A gyökér csomópont legyen kompozícióban a fával!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/UlTjUDckhqc">https://youtu.be/UlTjUDckhqc</link> 
        </para>
        <para>
            Megoldás forrása:
        </para> 
        <para> 
            <link xlink:href="Welch/z3a18qa5.cpp">bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Welch/z3a18qa5.cpp</link>       
        </para>
        <para>
            Ez az új, "from scratch" kód, a továbbiakban ezzel dolgozunk.
        </para>
        <para>
            Ebben az implementációban template osztályokat alkalmazunk, osztályon belül pedig konstruktorokat. A konstruktorok speciális függvények, melyek minden objektum létrehozása esetén meghívódnak. 
            Egy konstruktor neve megegyezik az osztály nevével. Visszatérési értéke nincs, de argumentumokat képes fogadni. Feladata az objektum értékeinek inicializálása. Egy osztályon belül több konstruktort is megadhatunk. 
        </para>
<programlisting language="c++"><![CDATA[
#include <iostream>
#include <random>
#include <functional>
#include <chrono>

class Unirand { //random fa építéséhez

    private:
        std::function <int()> random;

    public:
        Unirand(long seed, int min, int max): random(
            std::bind(
                std::uniform_int_distribution<>(min, max),
                std::default_random_engine(seed) 
            )
        ){}    

   int operator()(){return random();}
        
};

template <typename ValueType>
class BinRandTree {//fa osztály létrehozás

protected: 
    class Node { //beágyazott osztály a fa elemeinek -> protected, tehát elérhető olyan osztályokból is amik tőle örökölnek
        
    private:    //csak innen módosítható
        ValueType value;//ValueType típus, mivel template osztály
        Node *left;     //bal oldali gyerek mutatója
        Node *right;    //jobb oldali gyerek mutatója
        int count{0};   //számláló
        
        // TODO rule of five
        //mozgató szemantika miatt szükséges:
        Node(const Node &); //másoló konstruktor
        Node & operator=(const Node &); //másoló értékadás
        Node(Node &&);  //mozgató konstruktor
        Node & operator=(Node &&); //mozgató értékadás
        
    public: //nyilvános-> máshonnan is elérhető
        Node(ValueType value, int count=0): value(value), count(count), left(nullptr), right(nullptr) {}
        //függvények deklarálása
        ValueType getValue() const {return value;}
        Node * leftChild() const {return left;}
        Node * rightChild() const {return right;}
        void leftChild(Node * node){left = node;}
        void rightChild(Node * node){right = node;}
        int getCount() const {return count;}
        void incCount(){++count;}        
    };

    Node *root; //gyökér mutatója
    Node *treep; //fa mutatója
    int depth{0}; //fa mélysége
    
private:     
    // TODO rule of five
    
public:
    BinRandTree(Node *root = nullptr, Node *treep = nullptr): root(root), treep(treep) {
        std::cout << "BT ctor" << std::endl;        
    } //konstruktor, új fa létrehozása
    
    BinRandTree(const BinRandTree & old) { //másoló konstruktor
        std::cout << "BT copy ctor" << std::endl;
        
        root = cp(old.root, old.treep); //"old" fa gyökeréről másolat készül
    }
    
    Node * cp(Node *node, Node *treep) //fa elemeiről is másolat készül
    {
        Node * newNode = nullptr; //új elem létrehozása
        
        if(node)
        {
            newNode = new Node(node->getValue(), 42 /*node->getCount()*/);
            
            newNode->leftChild(cp(node->leftChild(), treep));//bal oldali részfa         
            newNode->rightChild(cp(node->rightChild(), treep));//jobb oldali részfa
            
            if(node == treep)
                this->treep = newNode;
        }
        
        return newNode; //visszatértünk az új elemmel
    }
    
    
    BinRandTree & operator=(const BinRandTree & old) {//másoló értékadás
        std::cout << "BT copy assign" << std::endl;
        
        BinRandTree tmp{old};//tmp(átmeneti) fa létrehozása az old mintájára (itt meghívódik a másoló konstruktor)
        std::swap(*this, tmp);
        return *this;
    }
    
    BinRandTree(BinRandTree && old) {//mozgató konstruktor
        std::cout << "BT move ctor" << std::endl;
        
        root = nullptr;//gyökeret nullptr-re állítjuk, mivel az old-ot már átmásoltuk, többé nem fogjuk használni
        *this = std::move(old);//ez a mozgató szemantika miatt szükséges, ezzel jelezzük hogy "old" eltávolításra kerül
        //rvalue referenciát hoz létre, ezzel biztosítva a mozgatást
    }
       
    BinRandTree & operator=(BinRandTree && old) { //mozgató értékadás
        std::cout << "BT move assign" << std::endl;
        
        std::swap(old.root, root);//felcseréljük a két gyökeret
        std::swap(old.treep, treep);//felcseréljük a két fa mutatóját
        
        return *this;
    }
    
    ~BinRandTree(){//destruktor
        std::cout << "BT dtor" << std::endl;
        deltree(root);//törlés
    }
    BinRandTree & operator<<(ValueType value);
    //függvények, fa kiírása és törlése
    void print(){print(root, std::cout);}
    void printr(){print(*root, std::cout);}
    void print(Node *node, std::ostream & os);
    void print(const Node &cnode, std::ostream & os);
    void deltree(Node *node); 

    Unirand ur{std::chrono::system_clock::now().time_since_epoch().count(), 0, 2}; //0 és 2 közötti számok generálása

    int whereToPut() {
    
            return ur();
    }

};
]]></programlisting> 
    </section>        
                
    <section>
        <title>Mutató a gyökér</title>
        <para>
            Írd át az előző forrást, hogy a gyökér csomópont ne kompozícióban, csak aggregációban legyen a 
            fával!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/UlTjUDckhqc">https://youtu.be/UlTjUDckhqc</link> 
        </para>
        <para>
            Megoldás forrása: <link xlink:href="Welch/z3a18qa5.cpp">bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Welch/z3a18qa5.cpp</link> 
        </para>
        <para>
            Lásd: 6.4
        </para>
    </section>                     

    <section>
        <title>Mozgató szemantika</title>
        <para>
            Tutoriáltam: <link xlink:href="https://gitlab.com/bh_konyv/bhax">Salánki László Balázs</link>
            </para>
        <para>
            Írj az előző programhoz mozgató konstruktort és értékadást, a mozgató konstruktor legyen a mozgató
            értékadásra alapozva!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/UlTjUDckhqc">https://youtu.be/UlTjUDckhqc</link> 
        </para>
        <para>
            Megoldás forrása: <link xlink:href="Welch/z3a18qa5.cpp">bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Welch/z3a18qa5.cpp</link> 
        </para>
        <para>
            Ebben a feladatban másoló konstruktort használunk, aminek az az elve, hogy már meglévő objektumokat használ fel újabb objektumok létrehozásához(ugyanazon az osztályon belül). Egy formális paramétere van, ami egyben az osztály típusa is. Másoló konstruktor akkor kerül meghívásra, mikor egy új objektumot hozunk létre egy már meglévő objektummal. 
            Mozgató konstruktort is alkalmazunk, ami a másoló konstruktorra van alapozva. A mozgató konstruktor egy osztály objektumának referenciáját veszi alapul. C++11 újításaként jelent meg és egy új referenciatípust hoz létre(rvalue), amit a "&amp;&amp;" jelöléssel tudunk alkalmazni. Ez átmeneti objektumokra hivatkozik, melyek úgy jöttek létre, hogy alkalmazható legyen rajtuk a mozgató szemantika. Ezeket az objektumokat csak úgy tárolhatjuk el, ha először átmásoljuk az értéküket(ezáltal egy új objektumot létrehozva). Ezután az átmeneti objektum törlődik a destruktor segítségével. A destruktor több esetben is meghívásra kerülhet, például ha véget ér a függvény vagy a program. 
        </para>
        <para>
            A mozgató konstruktor tehát képes egy átmeneti objektum értékét alapul venni ahhoz, hogy egy új objektumot hozzon létre ugyanazzal az értékekkel.  Miután ezt elvégezte, az átmeneti objektum mutatóját NULL-ra állítja. Ez azért hasznos, mert az átmeneti értéket már nem használjuk fel mégegyszer a programban, illetve nullpointerre se fogunk hivatkozni. Tehát törlés nélkül "eltávolítottuk" az objektumot. 
        </para>
        <para>
            Ahhoz, hogy alkalmazni tudjuk a mozgató szemantikát, az <command>std::move()</command> függvényt kell használnunk. Csak ekkor tudunk rvalue referenciát, azaz átmeneti objektumot létrehozni. 
        </para>
        <para>
           Tekintsük azt az esetet, mikor a main az alábbi módon néz ki:
        </para>
<programlisting language="c++"><![CDATA[
int main(int argc, char** argv, char ** env)
{//létrehozunk két fát, majd az egyiket átírjuk a másikra
    ZLWTree<char, '/', '0'> zt;
    ZLWTree<char, '/', '0'> zt2;
    zt=zt2; //másoló értékadás
}
]]></programlisting> 
        <para>
           Futtatás után ezt a kimenetet kapjuk:
        </para>
       <figure>
            <title></title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/lzw1.png" scale="70" />
                </imageobject>
                <textobject>
                    <phrase></phrase>
                </textobject>
            </mediaobject>
        </figure> 
        <para>
           Soronként tudjuk értelmezni, hogy mi történt. Először két objektumot hoztunk létre, zt-t és zt2-t, ezt a kimenet első két sora jelzi.
           A "BT copy assign"-t a másoló értékadás íratja ki. A következő sorban létrehozzuk az átmeneti objektumot, ezért a másoló konstruktor is meghívásra kerül. A másoló konstruktor íratja ki a következő sort: "BT copy ctor".
           Ezt követi a mozgató konstruktor, ami a "BT move ctor" szöveget írja ki. Ezután a move() függvénnyel ugrunk egyet a mozgató értékadásra, amit a "BT move assign" sor jelez nekünk. A következő két swap() miat ugyanez a sor még kétszer kiírásra kerül. Végül a utolsó négy sor a destruktorok működését jelzi, melyek a függvények végén kerülnek meghívásra.
        </para>
        <para>
            Amit fent részleteztem, az mind a BinRandTree osztály public részében található. A main-ben található értékadás miatt meghívásra kerül a másoló értékadás. Ezen belül a másoló konstruktor kerül meghívásra azért, mert egy új objektum jön létre, jelen esetben a <command>tmp</command>.
        </para>
<programlisting language="c++"><![CDATA[
        //másoló értékadás:
        BinRandTree & operator=(const BinRandTree & old)
        {
            std::cout << "BT copy assign" << std::endl;
        
            BinRandTree tmp{old}; //létrejön a tmp az old mintájára és meghívásra kerül a másoló konstuktor(gyökér másolása)
            std::swap(*this, tmp);
            return *this; 
        }        
]]></programlisting> 

<programlisting language="c++"><![CDATA[
        //másoló konstuktor:
        BinRandTree(const BinRandTree & old)
        {
            std::cout << "BT copy ctor" << std::endl;
        
            root = cp(old.root, old.treep); //gyökeret átmásoljuk
        }      
]]></programlisting> 

<programlisting language="c++"><![CDATA[
        //mozgató konstruktor:
        BinRandTree(BinRandTree && old)
        {
            std::cout << "BT move ctor" << std::endl;
        
            root = nullptr; 
            *this = std::move(old);
        } 
]]></programlisting> 

<programlisting language="c++"><![CDATA[
        //mozgató értékadás:
        BinRandTree(BinRandTree && old)
        BinRandTree & operator=(BinRandTree && old)
        {
            std::cout << "BT move assign" << std::endl;
        
            std::swap(old.root, root);
            std::swap(old.treep, treep);
        
            return *this;
        }
]]></programlisting>
    </section>   
    
    <section>
        <title>Red Flower Hell: 5x5x5 Observation from grid</title>
        <para>
            Megoldás videó:  <link xlink:href="https://youtu.be/3oi2CK1h7-w">https://youtu.be/3oi2CK1h7-w</link>
        </para>
        <para>
            Megoldás forrása: 
        </para>
        <para>
            <link xlink:href="Welch/5x5x5.py">
             <filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Welch/5x5x5.py</filename></link>
        </para>
        <para>
            Ebben a feladatban Steve több mindent érzékel maga körül. A körülötte lévő dolgokat nem 27, hanem 125, azaz 5x5x5-ös méretű tömbben tároljuk. Ehhez az xml-ben át kellett írnunk néhány értéket, illetve a Python kódban el kellett végeznünk a megfelelő módosításokat. (Tömb átnevezése, for ciklus kibővítése.)
        </para>
         <para>
             A feladat szemléltetésére az előző fejezetben felhasznált kódot módosítottam.
        </para>
<programlisting language="c++"><![CDATA[
def run(self):
        world_state = self.agent_host.getWorldState()
        felmegy = True //amíg ez a bool változó igaz, Steve megy fel
        # Loop until mission ends:
        while world_state.is_mission_running:
            print("--- nb4tf4i arena -----------------------------------\n")
            if world_state.number_of_observations_since_last_state != 0:
                
                sensations = world_state.observations[-1].text
                #print("    sensations: ", sensations)                
                observations = json.loads(sensations)
                nbr5x5x5 = observations.get("nbr5x5x5", 0)//tömb lekérése
                print("    5x5x5 neighborhood of Steve: ", nbr5x5x5)
                
                if "Yaw" in observations:
                    self.yaw = int(observations["Yaw"])
                if "Pitch" in observations:
                    self.pitch = int(observations["Pitch"])
                if "XPos" in observations:
                    self.x = int(observations["XPos"])
                if "ZPos" in observations:
                    self.z = int(observations["ZPos"])        
                if "YPos" in observations:
                    self.y = int(observations["YPos"])  
                
                print("    Steve's Coords: ", self.x, self.y, self.z)        
                print("    Steve's Yaw: ", self.yaw)        
                print("    Steve's Pitch: ", self.pitch)    

                //tömb vizsgálata
                for i in range (0,125):
                    if nbr5x5x5[i] == 'flowing_lava'://ha valahol a tömbben van láva:
                        if felmegy == True://ha éppen felfele megy:
                            //forduljon meg
                            self.agent_host.sendCommand( "turn 1" )
                            time.sleep(.01)
                            self.agent_host.sendCommand( "turn 1" )
                            time.sleep(.01)
                            felmegy = False//már ne menjen fel, forduljon vissza

                if felmegy == False://ha már megfordult, akkor csak menjen előre
                    self.agent_host.sendCommand( "move 1" )
                    time.sleep(.1)
                    
                else://ha még nem érkezett láva közelébe akkor ugráljon felfele
                    self.agent_host.sendCommand( "move 1" )
                    time.sleep(.1)
                    self.agent_host.sendCommand( "move 1" )
                    time.sleep(.1)
                    self.agent_host.sendCommand( "jumpmove 1" )
                    time.sleep(.2)
                
                
            world_state = self.agent_host.getWorldState()
]]></programlisting>

<para>
    Az átírt <command>xml</command> részlet:
</para>

<programlisting language="c++"><![CDATA[
<ObservationFromGrid>
    <Grid name="nbr5x5x5">
        <min x="-2" y="-2" z="-2"/>
        <max x="2" y="2" z="2"/>
    </Grid>
</ObservationFromGrid>
]]></programlisting>
        
        
    </section>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
</chapter>                
