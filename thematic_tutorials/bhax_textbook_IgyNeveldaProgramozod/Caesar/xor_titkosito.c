#include <stdio.h>
#include <string.h>

int main (int argc, char **argv)
{ 
    int  kulcs_hossz = strlen(argv[1]);     // argumentumként megadott kulcs hosszát meghatározzuk
    char kulcs[kulcs_hossz];                // a kulcs egy karakterekből álló tömb
    
    int  kulcs_index = 0;
    int  beolvasott_byte = 0;
    
    while( (beolvasott_byte = getchar()) != EOF)        //amíg be tudunk olvasni karaktert/amíg a végére nem érünk a szövegnek
    {
        beolvasott_byte = beolvasott_byte ^ argv[1][kulcs_index];   // XOR művelet a karakter és a kulcs között
        printf("%c", beolvasott_byte);                      //kiírjuk az eredményt
        
        kulcs_index++;
        kulcs_index %= kulcs_hossz;     //a következő kulcs elemre léptünk
    }
                                    //bármilyen (hosszúságú) kulcsot megadhatunk, mivel argumentumként kezeli és annak kéri le a hosszát
    return 0;
}
