#include <stdio.h>

int main()
{
    int a=5;
    int b=7;
    printf("%d %d\n",a,b);
    
    a = a + b;      //a= 12      b= 7
    b = a - b;      //a= 12      b= 5
    a = a - b;      //a= 7       b= 5
    printf("%d %d\n",a,b);

    return 0;
}
