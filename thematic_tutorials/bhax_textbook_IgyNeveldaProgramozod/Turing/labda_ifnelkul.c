#include <stdio.h>
#include <curses.h>
#include <unistd.h>

int main ()
{
    //Változók deklarálása
    int x = 0;
    int y = 0;
    int xnov = 1;
    int ynov = 1;
    int szelesseg,magassag;

    //Ablak méreteinek lekérdezése és eltárolása
    WINDOW *ablak;
    ablak = initscr();
    getmaxyx(ablak, szelesseg, magassag);
    
    for (;;)
    {
        mvprintw (x, y, "O");   //megjeleníti a labdát
        refresh();        
        usleep(25000);       //ezen érték módosításával állítható a labda mozgásának gyorsasága
        clear();    
        x += xnov;          //labda helyzetének változtatása
        y += ynov;
    
        for(int j=y; j>=0; j--)
        {
            ynov *= -1;
        }
        for(int j=y; j<=magassag; j++)
        {
            ynov *= -1;
        }
        for(int i=x; i>=0; i--)
        {
            xnov *= -1;
        }
        for(int i=x; i<szelesseg; i++)
        {
            xnov *= -1;
        }
        
    }
    
    return 0;
}
