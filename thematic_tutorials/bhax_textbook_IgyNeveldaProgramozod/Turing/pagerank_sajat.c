#include <stdio.h>
#include <math.h>

void matrix_kiir(double matrix[4][4]);
double pagerank_szamolas(double matrix[4][4]);

int main()
{
    double honlapok[4][4] = 
    {      //A     B     C     D
            {0.0, 0.0, 1/3.0, 0.0,},
            {0.5, 0.0, 1/3.0, 0.0,},
            {0.5, 0.0,  0.0,  1.0,},
            {0.0, 1.0, 1/3.0, 0.0 },
    };
    double pagerank_kezdeti[4] = {1/4.0, 1/4.0, 1/4.0, 1/4.0};
    
    for(int i=0; i<4; i++)
    {
        for(int j=0; j<4; j++)
        {
            honlapok[i][j] = honlapok[i][j]*honlapok[i][j];     //mátrixot négyzetre emelem
        }
    }
    //matrix_kiir(honlapok);
    
    double pagerank_vegleges[4];
    for(int i=0; i<4; i++)
    {
        double szum =0.0;
        for(int j=0; j<4; j++)
        {
            szum += honlapok[i][j] * pagerank_kezdeti[j];       //mátrixot megszorzom a vektorral
        }
        pagerank_vegleges[i] = szum;
    }
    
    for(int i=0; i<4; i++)
    {
        printf("%f ", pagerank_vegleges[i]);
    }

    return 0;
}

void matrix_kiir(double matrix[4][4])
{
    for(int i=0; i<4; i++)
    {
        for(int j=0; j<4; j++)
        {
            printf("%f ",matrix[i][j]);
        }
        printf("\n");
    }
};
