#include <stdio.h>
#include <math.h>

void kiir(double tomb[], int db)
{
    for (int i=0; i<db; i++)
    printf("PageRank [%d]: %lf\n", i, tomb[i]);
}

double tavolsag(double PR[],double PRv[],int db)
{
    double osszeg= 0.0;
    for(int i=0; i<db; i++)
    osszeg += (PR[i] - PRv[i]) * (PR[i] - PRv[i]);
    printf("osszeg=%f\n",osszeg);
    return sqrt(osszeg);
}

int main(void)
{
    double L[4][4] = {
                        {0.0, 0.0, 1.0 / 3.0, 0.0},
                        {1.0, 1.0 / 2.0, 1.0 / 3.0, 1.0},
                        {0.0, 1.0 / 2.0, 0.0, 0.0},
                        {0.0, 0.0, 1.0 / 3.0, 0.0}
                    };

    double PR[4] = {0.0, 0.0, 0.0, 0.0};
    double PR_tmp[4] = {1.0 / 4.0, 1.0 / 4.0, 1.0 / 4.0, 1.0 / 4.0};

    for (;;)
    {
        for(int i=0; i<4; i++)
            PR[i] = PR_tmp[i];
        for (int i=0;i<4;i++)
        {
            double tmp = 0;
            for (int j=0; j<4; j++)
            tmp += L[i][j] * PR[j];
            PR_tmp[i]=tmp;
        }

        if ( tavolsag(PR,PR_tmp, 4) < 0.000001)
            break;
    }

    kiir (PR,4);
    return 0;

} 
