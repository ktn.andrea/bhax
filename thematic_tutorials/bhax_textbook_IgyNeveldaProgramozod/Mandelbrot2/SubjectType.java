public enum SubjectType {
    THEORETICAL,
    PRACTICAL,
    LABORATORY
}
