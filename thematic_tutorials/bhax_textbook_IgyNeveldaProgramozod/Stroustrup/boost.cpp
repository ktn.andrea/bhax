#include <iostream>
#include <fstream>
#include <boost/filesystem.hpp>

using namespace std;

int main(){
    ofstream myfile;
    myfile.open("output.txt");
    
    using namespace boost::filesystem;
    recursive_directory_iterator dir("./"), end;
    int counter=0;
    
    while (dir != end)
    {
        if( dir->path().extension() == ".java" && is_regular_file(dir->path()) )
        {
            myfile << counter+1 << ". " << dir->path().filename().string() << endl;
            counter++;
        }
        dir++;
    }
    cout << "Total number of .java files in this library: " << counter << endl;

    myfile.close();
    return 0;
}
