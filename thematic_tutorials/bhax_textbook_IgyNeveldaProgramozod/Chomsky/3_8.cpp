#include <iostream>
using namespace std;

int *h()
{
    int x = 3;
    int *p = &x;
    cout<<p;
    return p;
}

int main ()
{
   
    int a = 5;
    cout<<"egész: "<<endl << a <<endl<<endl;
 
  
    int *b = &a; 
    cout<<"egészre mutató mutató: " <<endl << b <<endl<<endl;
    
  
    int &r = a;
    cout<<"egész referenciája: " <<endl << r <<endl<<endl;
    
    
  
    int c[5] = {1,2,3,4,5};
    cout<<"egészek tömbje: " <<endl;
    for(int i=0; i<5; i++)
    {
        cout<<c[i]<<' ';
    }
    cout<<endl<<endl;
    

    cout<<"egészek tömbjének referenciája: "<<endl;
    int (&tr)[5] = c;
    cout<<tr<<' '<<endl<<endl;
    
  
    
    cout<<"egészre mutató mutatók tömbje: "<<endl;
    int*d[5];
    for(int i=0; i<5; i++)
    {
        d[i] = &c[i];
        cout<< d[i] <<' ';
    }
    cout<<endl<<endl;
    
    
   
    cout<<"egészre mutató mutatót visszaadó függvény: "<< endl<< *h() <<endl<<endl;
    

    
    cout<< "függvény memóriacíme: " <<endl << h()<<endl;


} 
