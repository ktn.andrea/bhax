#include <iostream>
#include <random>
#include <functional>
#include <chrono>

class Unirand { //random fa építéséhez

    private:
        std::function <int()> random;

    public:
        Unirand(long seed, int min, int max): random(
            std::bind(
                std::uniform_int_distribution<>(min, max),
                std::default_random_engine(seed) 
            )
        ){}    

   int operator()(){return random();}
        
};

template <typename ValueType>
class BinRandTree {//fa osztály létrehozás

protected: 
    class Node { //beágyazott osztály a fa elemeinek -> protected, tehát elérhető olyan osztályokból is amik tőle örökölnek
        
    private:    //csak innen módosítható
        ValueType value;//ValueType típus, mivel template osztály
        Node *left;     //bal oldali gyerek mutatója
        Node *right;    //jobb oldali gyerek mutatója
        int count{0};   //számláló
        
        // TODO rule of five
        //mozgató szemantika miatt szükséges:
        Node(const Node &); //másoló konstruktor
        Node & operator=(const Node &); //másoló értékadás
        Node(Node &&);  //mozgató konstruktor
        Node & operator=(Node &&); //mozgató értékadás
        
    public: //nyilvános-> máshonnan is elérhető
        Node(ValueType value, int count=0): value(value), count(count), left(nullptr), right(nullptr) {}
        //függvények deklarálása
        ValueType getValue() const {return value;}
        Node * leftChild() const {return left;}
        Node * rightChild() const {return right;}
        void leftChild(Node * node){left = node;}
        void rightChild(Node * node){right = node;}
        int getCount() const {return count;}
        void incCount(){++count;}        
    };

    Node *root; //gyökér mutatója
    Node *treep; //fa mutatója
    int depth{0}; //fa mélysége
    
private:     
    // TODO rule of five
    
public:
    BinRandTree(Node *root = nullptr, Node *treep = nullptr): root(root), treep(treep) {
        std::cout << "BT ctor" << std::endl;        
    } //konstruktor, új fa létrehozása
    
    BinRandTree(const BinRandTree & old) { //másoló konstruktor
        std::cout << "BT copy ctor" << std::endl;
        
        root = cp(old.root, old.treep); //"old" fa gyökeréről másolat készül
    }
    
    Node * cp(Node *node, Node *treep) //fa elemeiről is másolat készül
    {
        Node * newNode = nullptr; //új elem létrehozása
        
        if(node)
        {
            newNode = new Node(node->getValue(), 42 /*node->getCount()*/);
            
            newNode->leftChild(cp(node->leftChild(), treep));//bal oldali részfa         
            newNode->rightChild(cp(node->rightChild(), treep));//jobb oldali részfa
            
            if(node == treep)
                this->treep = newNode;
        }
        
        return newNode; //visszatértünk az új elemmel
    }
    
    
    BinRandTree & operator=(const BinRandTree & old) {//másoló értékadás
        std::cout << "BT copy assign" << std::endl;
        
        BinRandTree tmp{old};//tmp(átmeneti) fa létrehozása az old mintájára (itt meghívódik a másoló konstruktor)
        std::swap(*this, tmp);
        return *this;
    }
    
    BinRandTree(BinRandTree && old) {//mozgató konstruktor
        std::cout << "BT move ctor" << std::endl;
        
        root = nullptr;//gyökeret nullptr-re állítjuk, mivel az old-ot már átmásoltuk, többé nem fogjuk használni
        *this = std::move(old);//ez a mozgató szemantika miatt szükséges, ezzel jelezzük hogy "old" eltávolításra kerül
        //rvalue referenciát hoz létre, ezzel biztosítva a mozgatást
    }
       
    BinRandTree & operator=(BinRandTree && old) { //mozgató értékadás
        std::cout << "BT move assign" << std::endl;
        
        std::swap(old.root, root);//felcseréljük a két gyökeret
        std::swap(old.treep, treep);//felcseréljük a két fa mutatóját
        
        return *this;
    }
    
    ~BinRandTree(){//destruktor
        std::cout << "BT dtor" << std::endl;
        deltree(root);//törlés
    }
    BinRandTree & operator<<(ValueType value);
    //függvények, fa kiírása és törlése
    void print(){print(root, std::cout);}
    void printr(){print(*root, std::cout);}
    void print(Node *node, std::ostream & os);
    void print(const Node &cnode, std::ostream & os);
    void deltree(Node *node); 

    Unirand ur{std::chrono::system_clock::now().time_since_epoch().count(), 0, 2}; //0 és 2 közötti számok generálása

    int whereToPut() {
    
            return ur();
    }

};


template <typename ValueType>
class BinSearchTree : public BinRandTree<ValueType> {

public:
    BinSearchTree() {}
    BinSearchTree & operator<<(ValueType value);
    
    
};

template <typename ValueType, ValueType vr, ValueType v0>
class ZLWTree : public BinRandTree<ValueType> {

public:
    ZLWTree(): BinRandTree<ValueType>(new typename BinRandTree<ValueType>::Node(vr)) {
        this->treep = this->root;
    }
    ZLWTree & operator<<(ValueType value);
    
    
};

template <typename ValueType>
BinRandTree<ValueType> & BinRandTree<ValueType>::operator<<(ValueType value)
{
    
    int rnd = whereToPut();
    
    if(!treep) {
       
        root = treep = new Node(value);
        
    } else if (treep->getValue() == value) {
        
        treep->incCount();
        
    } else if (!rnd) {
 
        treep = root;
        *this << value;
        
    } else if (rnd == 1) {
        
        if(!treep->leftChild()) {
            
            treep->leftChild(new Node(value));
            
        } else {
            
            treep = treep->leftChild();
            *this << value;
        }
        
    } else if (rnd == 2) {
        
        if(!treep->rightChild()) {
            
            treep->rightChild(new Node(value));
            
        } else {
            
            treep = treep->rightChild();
            *this << value;
        }
        
    }
        
    return *this;
}


template <typename ValueType>
BinSearchTree<ValueType> & BinSearchTree<ValueType>::operator<<(ValueType value)
{
    if(!this->treep) {
       
        this->root = this->treep = new typename BinRandTree<ValueType>::Node(value);
        
    } else if (this->treep->getValue() == value) {
        
        this->treep->incCount();
        
    } else if (this->treep->getValue() > value) {
        
        if(!this->treep->leftChild()) {
            
            this->treep->leftChild(new typename BinRandTree<ValueType>::Node(value));
            
        } else {
            
            this->treep = this->treep->leftChild();
            *this << value;
        }
        
    } else if (this->treep->getValue() < value) {
        
        if(!this->treep->rightChild()) {
            
            this->treep->rightChild(new typename BinRandTree<ValueType>::Node(value));
            
        } else {
            
            this->treep = this->treep->rightChild();
            *this << value;
        }
        
    }
    
    this->treep = this->root;
    
    return *this;
}


template <typename ValueType, ValueType vr, ValueType v0>
ZLWTree<ValueType, vr, v0> & ZLWTree<ValueType, vr, v0>::operator<<(ValueType value)
{
    
    if(value == v0) {
        
        if(!this->treep->leftChild()) {
            
            typename BinRandTree<ValueType>::Node * node = new typename BinRandTree<ValueType>::Node(value);
            this->treep->leftChild(node);
            this->treep = this->root;
            
        } else {
            
            this->treep = this->treep->leftChild(); 
        }
        
    } else {

        if(!this->treep->rightChild()) {
            
            typename BinRandTree<ValueType>::Node * node = new typename BinRandTree<ValueType>::Node(value);
            this->treep->rightChild(node);
            this->treep = this->root;
            
        } else {
            
            this->treep = this->treep->rightChild(); 
        }
        
    }
    
    return *this;
}

template <typename ValueType>
void BinRandTree<ValueType>::print(Node *node, std::ostream & os) 
{
    if(node)
    {
        ++depth;
        print(node->leftChild(), os);
        
        for(int i{0}; i<depth; ++i)
            os << "---";            
        os << node->getValue() << " " << depth << " " << node->getCount() << std::endl;     
        
        print(node->rightChild(), os);
        --depth;
    }
    
}


template <typename ValueType>
void BinRandTree<ValueType>::print(const Node &node, std::ostream & os) 
{

        ++depth;
        
        if(node.leftChild())
            print(*node.leftChild(), os);
        
        for(int i{0}; i<depth; ++i)
            os << "---";            
        os << node.getValue() << " " << depth << " " << node.getCount() << std::endl;     
        
        if(node.rightChild())
            print(*node.rightChild(), os);
        
        --depth;
    
}


template <typename ValueType>
void BinRandTree<ValueType>::deltree(Node *node) 
{
    if(node)
    {
        deltree(node->leftChild());
        deltree(node->rightChild());
        
        delete node;
    }
    
}


BinRandTree<int> bar()
{    
    BinRandTree<int> bt;
    BinRandTree<int> bt2;

    Unirand r(0, 0, 1);
    
    bt << 0 << 0 << 0;
    bt2 << 1 << 1 << 1;
    bt.print();
    std::cout << " --- " << std::endl;
    bt2.print();
    
    
    return r()?bt:bt2;
}



BinRandTree<int> foo()
{    
    return BinRandTree<int>();
}


int main(int argc, char** argv, char ** env)
{

    //fa létrehozása, elemei random számok 0 és 2 között
    BinRandTree<int> bt2{bar()};
    std::cout << " *** " << std::endl;
    bt2.print();
    std::cout << std::endl;

    ZLWTree<char, '/', '0'> zt;
    ZLWTree<char, '/', '0'> zt2;
    zt=zt2; //másoló értékadás
    std::cout << std::endl;
    std::cout<< std::endl;
    
  /*  //ZLW fák létrehozása, másolása
    ZLWTree<char, '/', '0'> zt;
    zt <<'0'<<'1'<<'0'<<'0'<<'1' << '1'; //elemek megadása
    zt.print(); //kiírás
    
    ZLWTree<char, '/', '0'> zt2{zt};
    ZLWTree<char, '/', '0'> zt3;

    zt3 <<'1'<<'1'<<'1'<<'1'<<'1';

    std::cout << "***" << std::endl;
    zt = zt3; //másoló értékadás
    std::cout << "***" << std::endl;

    ZLWTree<char, '/', '0'> zt4 = std::move(zt2);
*/
    //fa létrehozása, elemei string-ek
    BinSearchTree<std::string> bts;
    bts << "alma" << "korte" << "banan" << "korte";
    bts.print();    

}
