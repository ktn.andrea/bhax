Magasszintű programozási nyelvek 1-2 tárgyakhoz készített beadandó munkám.
==========================================================================

Az elkészített könyv elérési útvonala:

**https://gitlab.com/ktn.andrea/bhax/-/raw/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/bhax-textbook-fdl.pdf**

*Debreceni Egyetem Informatikai Kar*
